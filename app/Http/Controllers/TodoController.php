<?php

namespace App\Http\Controllers;

use App\Http\Requests\Todo\StoreRequest;
use App\Http\Requests\Todo\UpdateRequest;
use App\Models\Todo;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Todo::get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $todo = Todo::create([
                'task' => $request->get('task'),
                'done' => false,
            ]);

            return response()->success('todo', $todo);
        } catch (\Exception $e) {
            logger($e, compact('request'));

            return response()->error($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(UpdateRequest $request, Todo $todo)
    {
        try {
            $todo = $todo->fill([
                'done' => $request->get('done', $todo->done),
            ]);

            $todo->save();

            return response()->success('todo', $todo);
        } catch (\Exception $e) {
            logger($e, compact('request'));

            return response()->error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Todo $todo)
    {
        try {
            $todo->delete();

            return response()->success('');
        } catch (\Exception $e) {
            logger($e, compact('todo'));

            return response()->error($e->getMessage());
        }
    }
}
