class TodosController{
    constructor(API, ToastService){
        'ngInject';

        this.API = API;
        this.ToastService = ToastService;
    }

    $onInit(){
        this.todos = [];

        this.loadTodos();
    }

    loadTodos(){
        this.API.all('todo').getList().then((response) => {
            this.todos = response.plain();
        });
    }

    markAsDone(todo){
        var item = this.API.one('todo', todo.id);

        item.done = todo.done ? 1 : 0;

        item.put().then((response) => {
            this.ToastService.show('Task status updated.');
        });
    }

    destroy(todo){
        this.API.one('todo', todo.id).remove().then((response) => {
            this.loadTodos();
            this.ToastService.show('Task deleted.');
        });
    }

    submit(){
        var data = {
            task: this.task
        };

        this.API.all('todo').post(data).then((response) => {
            this.ToastService.show('Todo added successfully');
            this.loadTodos();
            this.task = null;
        }, (response) => {
            this.ToastService.error('Error occurred!');
        });
    }
}

export const TodosComponent = {
    templateUrl: './views/app/components/todos/todos.component.html',
    controller: TodosController,
    controllerAs: 'vm',
    bindings: {}
}
